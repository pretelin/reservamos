
export default async function handler(req, res) {
  const { city } = req.query;

  if (!city) {
    return res.status(400).json({ message: "City name is required" });
  }

  try {
    const response = await fetch(`https://search.reservamos.mx/api/v2/places?q=${city}`);
    const data = await response.json();

    const cities = data.filter(place => place.result_type === "city");

    res.status(200).json(cities);
  } catch (error) {
    res.status(500).json({ message: "Error fetching data from Reservamos API", error });
  }
}
