// pages/api/weatherForecast.js

export default async function handler(req, res) {
  const { lat, long } = req.query;
  const apiKey = process.env.OPENWEATHER_API_KEY; // Store your API key in an environment variable

  if (!lat || !long) {
    return res.status(400).json({ message: "City name is required" });
  }

  const url = `https://api.openweathermap.org/data/2.5/forecast?lat=${lat}&lon=${long}&appid=${apiKey}&units=metric`;

  try {
    const response = await fetch(url);
    const data = await response.json();

    if (!response.ok) {
      // Forward the error message from OpenWeatherMap API
      res.status(response.status).json({ message: data.message });
    } else {
      res.status(200).json(data);
    }
  } catch (error) {
    res.status(500).json({ message: "Error fetching weather data", error: error.message });
  }
}
