'use client';

import { useState, useEffect, useCallback } from 'react';
import { Container, Autocomplete, Text } from '@mantine/core';
import { useFetchDestinations } from './hooks/useFetchDestinations';
import WeatherData from './components/WeatherData';
import { fetchWeatherForecast } from './services/index';

export default function App() {
  const [userInput, setUserInput] = useState('');
  const [selection, setSelection] = useState(null);
  const [destinations, debouncedFetchDestinations] = useFetchDestinations();
  const [forecast, setCurrentForecast] = useState(null);

  useEffect(() => {
    debouncedFetchDestinations(userInput);
  }, [userInput, debouncedFetchDestinations]);

  const handleWeatherForecast = useCallback(async () => {
    const destinationSelected = destinations.find(d => d.value === selection)?.data;
    const forecastData = await fetchWeatherForecast(destinationSelected.lat, destinationSelected.long);
    
    setCurrentForecast(forecastData);
    return;
  }, [selection, destinations]);

  useEffect(() => {
    if (!selection) return;

    handleWeatherForecast();
  }, [selection]);

  return (
    <Container>
      <Container mt={40} mb={20}>
        <Text size="lg" fw={700}>
          Manuel Pretelin / Code Challenge
        </Text>
      </Container>
      <Container mt={20}>
        <Text>Check the weather forecast for the destinations available in <b>Reservamos.</b></Text>
        <Autocomplete
          label="Your favorite destination"
          placeholder="Pick value or enter anything"
          data={destinations}
          value={userInput}
          onChange={setUserInput}
          onOptionSubmit={setSelection}
          mt={10}
        />
      </Container>
      {selection && (
        <Container mt={20}>
          <WeatherData data={forecast} />
        </Container>
      )}
    </Container>
  );
}
