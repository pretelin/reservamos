export const fetchWeatherForecast = async (lat, long) => {
  if (!lat || !long) {
    return;
  }
  
  const response = await fetch(`/api/weather?lat=${lat}&long=${long}`);

  if (!response.ok) {
    throw new Error('Network response was not ok');
  }
  const data = await response.json();

  return data;
}