'use client';

import { MantineProvider } from '@mantine/core';
import App from './App';

export default function Home() {
  return (
    <MantineProvider defaultColorScheme="light">
      <App />
    </MantineProvider>
  )
}
