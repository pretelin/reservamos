// hooks/useFetchDestinations.js
import { useState, useCallback } from 'react';
import debounce from 'lodash.debounce';

export const useFetchDestinations = () => {
  const [destinations, setDestinations] = useState([]);

  const fetchDestinations = async (city) => {
    if (!city) {
      setDestinations([]);
      return
    };

    try {
      const response = await fetch(`/api/destinations?city=${encodeURIComponent(city)}`);
      if (!response.ok) {
        throw new Error('Network response was not ok');
      }
      const data = await response.json();

      setDestinations(data.map(d => ({
        value: String(d.id),
        label: `${d.city_name}, ${d.state}`,
        data: d
      })) );
    } catch (error) {
      console.error('Failed to fetch destinations:', error);
    }
  };

  const debouncedFetchDestinations = useCallback(debounce(fetchDestinations, 300), []);

  return [destinations, debouncedFetchDestinations];
};
