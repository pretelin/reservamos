import React from 'react';
import { Carousel } from '@mantine/carousel';
import { Card, Flex, Text, Loader, Center } from '@mantine/core';
import { splitDataByDay, formatDate, WEATHER_EMOJIS } from '../utils/index';

const WeatherData = ({ data }) => {
  const [days, setDays] = React.useState(null);

  React.useEffect(() => {
    const daysData = splitDataByDay(data);
    setDays(daysData);
  }, [data?.city?.id, data]);

  if (!days || !data?.city?.name) {
    return <Center>
      <Loader />
    </Center>;
  }

  return (
    <div>
      <Text style={{ textAlign: "center" }} mb={30}>Weather Forecast for <b>{data?.city?.name}, {data?.city?.country}</b></Text>
      <Carousel withIndicators>
        {Object.entries(days).map(([date, forecasts], index) => (
          <Carousel.Slide key={index}>
            <Center>
              <Card>
                <Flex direction={"column"}>
                  <Text fw={600} size="xl" mb={20}>{formatDate(date)}</Text>
                  {[forecasts[0]].map((forecast, fIndex) => (
                    <Flex key={fIndex} direction="column" align="center">
                      <div style={{marginRight: 20}}>
                        <Text size={"100px"}>{WEATHER_EMOJIS[forecast.weather[0].main] || WEATHER_EMOJIS.default}</Text>
                      </div>
                      <Text size={"50px"} fw={700}>
                        {Math.trunc(Number(forecast.main.temp_min))}° / {Math.trunc(Number(forecast.main.temp_max))}°
                      </Text>
                      <Text size={"100px"} fw={700}></Text>
                    </Flex>
                  ))}
                </Flex>
              </Card>
            </Center>
          </Carousel.Slide>
        ))}
      </Carousel>
    </div>
  );
};

export default WeatherData;
