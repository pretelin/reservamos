export const WEATHER_EMOJIS = {
  Rain: '🌧️',
  Snow: '❄️',
  Clouds: '☁️',
  Clear: '☀️',
  Thunderstorm: '⛈️',
  Drizzle: '🌦️',
  Mist: '🌫️',
  default: "🙂"
}

export const splitDataByDay = (forecastData) => {
  if (!forecastData) {
    return {};
  }

  return forecastData.list.reduce((acc, item) => {
    const date = item.dt_txt.split(' ')[0];

    if (!acc[date]) {
      acc[date] = [];
    }

    acc[date].push(item);
    return acc;
  }, {});
};

export const formatDate = (dateString) => {
  const date = new Date(dateString);
  const optionsDay = { day: 'numeric' };
  const optionsWeekday = { weekday: 'short' };

  const day = date.toLocaleDateString('en-US', optionsDay);
  const weekday = date.toLocaleDateString('en-US', optionsWeekday);

  return `${weekday} ${day}`;
}